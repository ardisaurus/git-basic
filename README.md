# Git Basic

### Git stages
1. modified
2. staging 
3. commited

### Create Repository
```sh
$ git init
```

create .git directory that stored every changelog

```sh
$ git status
```

check every changes and compare it to staging area

### Staging
```sh
$ git add .
```

add every changes to staging area

```sh
$ git add <filename.extention>
```

add changes from a file to staging area. Example : `git add index.html`

```sh
$ git rm --cached <filename.extention>
```

remove changes in index.html from staging area. Example : `git rm --cached index.html`

### Making commit
```sh
$ git commit –m “<message>”
```

create commit that save everything in staging area to repository. Example : `git commit -m "bug fixed"`

```sh
$ git log
```

show every commit from history

```sh
$ git log --oneline
```

show short version of log

### Undoing things
```sh
$ git checkout <commitlognumber>
```

go back to stage of commit in the past without changing present stage. Example : `git checkout af6b84c`

```sh
$ git revert <commitlognumber>
```

revert every changes to past stage. Example : `git revert af6b84c`

### Branch
```sh
$ git branch –a
```

show all branch

```sh
$ git branch <newbranchname>
```

create new branch. Example : `git branch feature2`

```sh
$ git checkout <branchname>
```

switch to other branch. Example : `git checkout feature2`

```sh
$ git branch -d <branchname>
```

delete branch if branch already merged to master. Example : `git branch -d feature2`

```sh
$ git branch -D <branchname>
```

delete without checking brach merge status. Example : `git branch -D feature2`

```sh
$ git checkout –b <branchname>
```

create new branch and switch to it. Example : `git branch -b feature2`

```sh
$ git merge <branchname>
```

merge changes in branch to master branch. need to be in master branch first. type of merge : fast-forward = master branch not changed after branch created, recursive = master branch changed after branch created. Example : `git merge feature2`

### Remote Repository
```sh
$ git remote add origin http://github.com/ardisaurus/testxxx
```

add remote repository to setting. Example : `git remote add origin https://github.com/ardisaurus/testxxx`

```sh
$ git pull origin master
```

update every changes from master branch on remote repository to local repository.

```sh
$ git push origin master
```

save every commited change to remote repository in master branch

```sh
$ git clone <remoterepository>
```

copy repository from remote repository. Example : `git clone https://github.com/ardisaurus/testxxx`

### Create pull request
1. Make new branch of existing repository, make some changes, and create pull request.
2. Fork a repository, make some changes, and create pull request.
